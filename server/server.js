import { ApolloServer } from "@apollo/server";
import { startStandaloneServer } from "@apollo/server/standalone";

const mockNFTs = [];

// Starting value for the random query parameter
let randomQueryNumber = 1;

// Number of NFTs we want to mock
const numNFTs = 15;

for (let i = 1; i <= numNFTs; i++) {
  const nft = {
    id: i.toString(),
    name: `NFT ${i}`,
    price: i * 5,
    image: `https://picsum.photos/800?random=${randomQueryNumber}`,
    description: `Description for NFT ${i}`,
    rating: (Math.random() * 4 + 1).toFixed(1)
  };

  randomQueryNumber++;

  mockNFTs.push(nft);
}

// GraphQL schema
const typeDefs = `
  type NFT {
    id: ID!
    name: String!
    price: Float!
    image: String!
    description: String
    rating: Float
   
  }
  type NFTList {
    items: [NFT]
    totalCount: Int
  }

  type Query {
    nfts(page: Int!, perPage: Int!): NFTList
    nft(nftId: ID!): NFT
  }
`;

const resolvers = {
  Query: {
    nfts: (_, args) => {
      const { page, perPage } = args;
      const startIndex = (page - 1) * perPage;
      const endIndex = startIndex + perPage;
      const paginatedNFTs = mockNFTs.slice(startIndex, endIndex);

      return {
        items: paginatedNFTs,
        totalCount: mockNFTs.length
      };
    },
    nft: (_, args) => {
      const nft = mockNFTs.find((nft) => nft.id === args.nftId);
      if (!nft) {
        return null;
      }
      return nft;
    }
  }
};

// Create an Apollo Server instance
const server = new ApolloServer({ typeDefs, resolvers });

// Start the server on specific port
startStandaloneServer(server, {
  listen: { port: 4000 }
}).then(({ url }) => {
  console.log(`Server is running at ${url}`);
});
