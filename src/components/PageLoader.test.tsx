import { render } from "@testing-library/react";
import { PageLoader } from "./PageLoader";

test("should render PageLoader component with CircularProgress", () => {
  // Render the PageLoader component
  const { getByTestId } = render(<PageLoader />);

  // Check if CircularProgress component is present within the PageLoader
  const circularProgress = getByTestId("circular-progress");
  expect(circularProgress).toBeInTheDocument();
});
