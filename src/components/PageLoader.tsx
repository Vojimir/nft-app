import CircularProgress from "@mui/material/CircularProgress";
import styled from "@emotion/styled";

export const PageLoader = () => {
  return (
    <CenteredContainer>
      <CircularProgress data-testid="circular-progress" />
    </CenteredContainer>
  );
};

const CenteredContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`;
