import { render, screen, waitFor, act } from "@testing-library/react";
import { ErrorSnackbar } from "./ErrorSnackbar";

test("should render an alert with error message and close when the handleClose function is called", async () => {
  const error = "This is an error message";

  act(() => {
    render(<ErrorSnackbar error={error} />);
  }); // Render the component within an act

  const alert = screen.getByText(error); // Use screen to get the alert
  expect(alert).toBeInTheDocument();

  const closeButton = screen.getByTitle("Close"); // Get the button by its title

  act(() => {
    closeButton.click(); // Click the "Close" button within an act
  });

  // Wait for the element to be removed
  await waitFor(() => {
    expect(screen.queryByText(error)).not.toBeInTheDocument(); // Ensure it's no longer in the document
  });
});
