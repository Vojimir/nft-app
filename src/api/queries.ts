import { gql } from "@apollo/client";

// GraphQL queries
export const GET_NFTS = gql`
  query GetNFTs($page: Int!, $perPage: Int!) {
    nfts(page: $page, perPage: $perPage) {
      items {
        id
        name
        price
        image
      }
      totalCount
    }
  }
`;

export const GET_NFT_BY_ID = gql`
  query GetNFT($id: ID!) {
    nft(nftId: $id) {
      id
      name
      price
      image
      description
      rating
    }
  }
`;
