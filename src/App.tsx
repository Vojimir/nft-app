import { Routes, Route } from "react-router-dom";
import Homepage from "./pages/Homepage";
import NftDetailsPage from "./pages/NftDetailsPage";

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Homepage />} />
        <Route path="/nft/:id" element={<NftDetailsPage />} />
        <Route path="*" element={<Homepage />} />
      </Routes>
    </>
  );
}

export default App;
