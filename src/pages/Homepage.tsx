import { useQuery } from "@apollo/client";
import { GET_NFTS } from "../api/queries";
import { PageLoader } from "../components/PageLoader";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { ErrorSnackbar } from "../components/ErrorSnackbar";
import { Box, Grid, Pagination, Paper } from "@mui/material";
import { useState } from "react";
import { Link } from "react-router-dom";

interface NftType {
  id: string;
  name: string;
  price: number;
  image: string;
}

const Homepage = () => {
  const [page, setPage] = useState(1);
  const nftsPerPage = 5;

  const { data, loading, error } = useQuery(GET_NFTS, {
    variables: {
      page: page,
      perPage: nftsPerPage
    }
  });

  if (error) {
    return <ErrorSnackbar error={error.message} />;
  }

  if (loading) {
    return <PageLoader />;
  }

  if (data && data.nfts.items.length > 0) {
    return (
      <Box display="flex" justifyContent="center">
        <Paper
          sx={{
            padding: 4,
            margin: 4,
            maxWidth: "1440px"
          }}
        >
          <Typography variant="h3" component="div" textAlign="center" mb={6}>
            NFTs
          </Typography>
          <Grid container spacing={4} textAlign="center">
            {data.nfts.items.map((nft: NftType) => (
              <Grid item key={nft.id} xs={12} sm={12} md={6} lg={4}>
                <Box display="flex" justifyContent="center">
                  <Card sx={{ maxWidth: 300 }}>
                    <Link
                      to={`/nft/${nft.id}`}
                      style={{ color: "inherit", textDecoration: "none" }}
                    >
                      <img
                        src={nft.image}
                        alt={nft.name}
                        style={{ width: 300, height: 300 }}
                      />
                      <CardContent>
                        <Typography variant="h5" component="div">
                          {nft.name}
                        </Typography>
                        <Typography color="text.secondary">
                          Price: ${nft.price}
                        </Typography>
                      </CardContent>
                    </Link>
                  </Card>
                </Box>
              </Grid>
            ))}
          </Grid>

          <Box mt={6} display="flex" justifyContent="center">
            <Pagination
              count={Math.ceil(data.nfts.totalCount / nftsPerPage)}
              page={page}
              onChange={(event, value) => setPage(value)}
            />
          </Box>
        </Paper>
      </Box>
    );
  }

  return (
    <ErrorSnackbar
      error={
        "No data available. Please ensure that the server is up and running."
      }
    />
  );
};

export default Homepage;
