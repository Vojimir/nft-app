import { useParams, useNavigate } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { GET_NFT_BY_ID } from "../api/queries";
import { Box, Paper, Typography } from "@mui/material";
import { ErrorSnackbar } from "../components/ErrorSnackbar";
import { PageLoader } from "../components/PageLoader";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import IconButton from "@mui/material/IconButton";

const NftDetailsPage = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  const handleBackClick = () => {
    navigate(-1);
  };

  const { data, loading, error } = useQuery(GET_NFT_BY_ID, {
    variables: { id: String(id) }
  });

  if (error) {
    return <ErrorSnackbar error={error.message} />;
  }

  if (loading) {
    return <PageLoader />;
  }

  if (data && data.nft) {
    return (
      <Box display="flex" justifyContent="center">
        <Paper
          sx={{
            padding: 4,
            margin: 4,
            maxWidth: "1440px"
          }}
        >
          <IconButton aria-label="delete" size="large">
            <ArrowBackIcon onClick={handleBackClick} fontSize="inherit" />
          </IconButton>
          <Typography variant="h3" component="div" textAlign="center" mb={6}>
            NFT Details Page
          </Typography>
          <Box>
            <Typography variant="h5">{data.nft.name}</Typography>
            <Typography>Price: ${data.nft.price}</Typography>
            <Typography>Description: {data.nft.description}</Typography>{" "}
            <Typography>Rating: {data.nft.rating} / 5</Typography>{" "}
            <img src={data.nft.image} alt={data.nft.name} />
          </Box>
        </Paper>
      </Box>
    );
  }

  return (
    <ErrorSnackbar
      error={
        "No data available. Please ensure that the server is up and running."
      }
    />
  );
};

export default NftDetailsPage;
